# Work with Index

**Ohne Index**

```sql
EXPLAIN ANALYZE 
select *
  from log join datapoints dp on dp.id=iddatapoints
```

Execution Time: 19854.771 ms

**Mit Index**

![image-20230320151923457](assets/image-20230320151923457.png)

```sql
EXPLAIN ANALYZE 
select *
  from log join datapoints dp on dp.id=iddatapoints
```

Execution Time: 19732.517 ms