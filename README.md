# Postgres mit Docker

## Where do I start
Whenever you work with Docker Images, the best way to start is the Docker Hub website. Just google for whatever you want to containerize and add "Docker" to it. For PostgreSQL this brings you to
- [postgres - Official Image | Docker Hub](https://hub.docker.com/_/postgres)

This site shows you all the possible ways to run this image.

Preparation

Clone this repository and change folder

```sh
git clone https://gitlab.com/bbwin/m141-postgres
cd m141-postgres
```

Folder should look like this

![image-20230320144757727](assets/image-20230320144757727.png)

Start docker

```sh
docker compose up -d
```

Connect to the logs
```sh
docker compose logs -f
```

```sh
m141-postgres-db-1       | PostgreSQL init process complete; ready for start up.
```

You can access adminer on [http://localhost:8080/?pgsql=db&username=postgres&db=synco&ns=public](http://localhost:8080/?pgsql=db&username=postgres&db=synco&ns=public). Use the password `example` (you can set it with an ENV variable in docker-compose.yml)

## Interaktion mit Docker
```sh
docker compose exec -it db bash
```

```sh
root@29029754c031:/# psql --username postgres --db-name synco
```